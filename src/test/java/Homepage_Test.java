import cz.cvut.fel.ts1.page_objects.Homepage_Page;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Homepage_Test {
    static WebDriver driver;

    @BeforeAll
    public static void signUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\kristov\\Documents\\ts1-hw03-selenium\\src\\chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void goToLoginPageTest() {
        driver.get("https://link.springer.com/");

        Homepage_Page homepage_page = new Homepage_Page(driver);

        homepage_page.goToLoginPage();
    }

    @Test
    public void goToAdvancedSearchTest() {
        driver.get("https://link.springer.com/");

        Homepage_Page homepage_page = new Homepage_Page(driver);

        homepage_page.goToAdvancedSearch();
    }
}

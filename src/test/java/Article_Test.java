import cz.cvut.fel.ts1.page_objects.Article_Page;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Article_Test {
    static WebDriver driver;

    @BeforeAll
    public static void signUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\kristov\\Documents\\ts1-hw03-selenium\\src\\chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void getTitleDoiDate() {
        driver.get("https://link.springer.com/article/10.1557/mre.2020.42");
        Article_Page article_page = new Article_Page(driver);
        //article_page.cookieBtn();
        article_page.findTitleDOIandDate();
    }
}

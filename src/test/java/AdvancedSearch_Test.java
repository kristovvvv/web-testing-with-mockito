import cz.cvut.fel.ts1.page_objects.AdvancedSearch_Page;
import cz.cvut.fel.ts1.page_objects.Signup_Page;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AdvancedSearch_Test {

    static WebDriver driver;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\kristov\\Documents\\ts1-hw03-selenium\\src\\chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void fillAndSubmitFormTest() {
        driver.get("https://link.springer.com/advanced-search");
        String allWords = "Page Object Model";
        String leastWords = "Selenium Testing";
        String startYear = "2021";
        String endYear = "2021";
        String choose = "Article";

        AdvancedSearch_Page advancedSearch_page = new AdvancedSearch_Page(driver);

        advancedSearch_page.fillForm(allWords, leastWords, startYear, endYear);

        advancedSearch_page.cookieBtn();

        advancedSearch_page.clickBtn();
    }
}

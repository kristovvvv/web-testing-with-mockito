import cz.cvut.fel.ts1.page_objects.Signup_Page;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Signup_Test {
    static WebDriver driver;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\kristov\\Documents\\ts1-hw03-selenium\\src\\chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void login_test() {
        driver.get("https://link.springer.com/signup-login");

        String email = "krystof.mueller@seznam.cz";
        String password = "Bdnhg123";

        Signup_Page signup_page = new Signup_Page(driver);
        signup_page.fillOutSignInForm(email, password);
        signup_page.sendForm();
    }

}

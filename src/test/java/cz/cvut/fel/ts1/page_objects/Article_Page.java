package cz.cvut.fel.ts1.page_objects;

import org.junit.platform.commons.logging.LoggerFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.logging.Logger;

public class Article_Page {
    private WebDriver driver;

    public Article_Page(WebDriver driver) {
        this.driver = driver;
    }

    private By title = By.className("c-article-title");
    private By DOI = By.cssSelector(".c-bibliographic-information__value > a");
    private By published = By.cssSelector("header > ul > li:nth-child(2) > a");

    public void findTitleDOIandDate() {
        String DOI = getDOI();
        String currentUrl = driver.getCurrentUrl();
        currentUrl += "#article-info";
        driver.get(currentUrl);
    }

    public String getDOI() {
        String DOI = driver.getCurrentUrl();
        String[] plsWork = DOI.split("/article/",2);
        DOI = plsWork[1];
        return DOI;
    }

    public String getTitleName() {
        String titleName = driver.findElement(title).getText();
        return titleName;
    }

    public String getPublishedDate() {
        String publishedDate = driver.findElement(published).getText();
        return publishedDate;
    }
}

package cz.cvut.fel.ts1.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Signup_Page {
    private WebDriver driver;

    private By email_input = By.id("login-box-email");
    private By pw_input = By.id("login-box-pw");
    private By confirmLogin_submitButton = By.cssSelector(".form-submit > button");

    public Signup_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void fillOutSignInForm(String email, String password) {
        driver.findElement(email_input).sendKeys(email);
        driver.findElement(pw_input).sendKeys(password);
    }

    public void sendForm() {
        driver.findElement(confirmLogin_submitButton).click();
    }
}

package cz.cvut.fel.ts1.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdvancedSearch_Page {
    private WebDriver driver;

    private By allWords_input = By.id("all-words");
    private By leastWords_input = By.id("least-words");
    private By startYear_input = By.id("facet-start-year");
    private By endYear_input = By.id("facet-end-year");
    private By submitBtn = By.id("submit-advanced-search");


    public AdvancedSearch_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void fillForm(String allWords, String leastWords, String startYear, String endYear) {
        driver.findElement(allWords_input).sendKeys(allWords);
        driver.findElement(leastWords_input).sendKeys(leastWords);
        driver.findElement(startYear_input).sendKeys(startYear);
        driver.findElement(endYear_input).sendKeys(endYear);
    }

    public void clickBtn() { driver.findElement(submitBtn).click(); }

    private By cookieBtn = By.id("onetrust-accept-btn-handler");
    public void cookieBtn() { driver.findElement(cookieBtn).click(); }
}

package cz.cvut.fel.ts1.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Search_Page {
    private WebDriver driver;
    private By query = By.id("query");

    public Search_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void chooseContentTypeOfResults(String contentTypeText) {
        WebElement e = driver.findElement(By.xpath("//*[text()='"+contentTypeText+"']"));
        e.click();
    }

    public void chooseArticle(Integer numberOfArticle) {
        By article = By.cssSelector("main > ol > li:nth-child("+numberOfArticle+") > h2 > a");
        driver.findElement(article).click();
    }

    public void searchArticle(String article) {
        String currentUrl = driver.getCurrentUrl();
        currentUrl += "?query=";
        currentUrl += article;
        driver.navigate().to(currentUrl);
    }

    private By cookieBtn = By.id("onetrust-accept-btn-handler");
    public void cookieBtn() { driver.findElement(cookieBtn).click(); }

}

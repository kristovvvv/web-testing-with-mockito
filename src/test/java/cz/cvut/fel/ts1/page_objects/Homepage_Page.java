package cz.cvut.fel.ts1.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Homepage_Page {

    private WebDriver driver;


    public Homepage_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void goToLoginPage() {
        driver.navigate().to("https://link.springer.com/signup-login");
    }

    public void goToAdvancedSearch() {
        driver.navigate().to("https://link.springer.com/advanced-search");
    }

    private By cookieBtn = By.id("onetrust-accept-btn-handler");
    public void cookieBtn() { driver.findElement(cookieBtn).click(); }
}

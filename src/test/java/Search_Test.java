import cz.cvut.fel.ts1.page_objects.Search_Page;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Search_Test {
    static WebDriver driver;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\kristov\\Documents\\ts1-hw03-selenium\\src\\chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void chooseContentTypeArticle() {
        driver.get("https://link.springer.com/search");
        String contentType = "Article";
        Search_Page search_page = new Search_Page(driver);
        search_page.cookieBtn();
        search_page.chooseContentTypeOfResults(contentType);
        search_page.chooseArticle(1);
    }

}

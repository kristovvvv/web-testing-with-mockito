package FINAL_TESTS;

import cz.cvut.fel.ts1.page_objects.Article_Page;
import cz.cvut.fel.ts1.page_objects.Search_Page;
import cz.cvut.fel.ts1.page_objects.Signup_Page;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class SavedArticlesTest {

    static WebDriver driver;
    String[] Dois = new String[] {"10.1007/s10846-021-01312-6", "10.1007/s10032-021-00362-8", "10.1007/s11786-021-00516-1", "10.1007/s10055-020-00433-x"};
    String[] Dates = new String[] {"Published: 22 February 2021", "Published: 03 March 2021", "Published: 10 May 2021", "Published: 23 April 2020"};
    String[] Titles = new String[] {"Endowing Robots with Longer-term Autonomy by Recovering from External Disturbances in Manipulation Through Grounded Anomaly Classification and Recovery Policies", "Combination of deep neural networks and logical rules for record segmentation in historical handwritten registers using few examples", "Correction to: Factorials Experiments, Covering Arrays, and Combinatorial Testing", "Motivations, design, and preliminary testing for a 360° vision simulator"};

    public SavedArticlesTest() {

    }

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\kristov\\Documents\\ts1-hw03-selenium\\src\\chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void foundArticlesParametersEqualInsertedParameters() {
        driver.get("https://link.springer.com/signup-login");
        Signup_Page signup_page = new Signup_Page(driver);
        signup_page.fillOutSignInForm();
        signup_page.sendForm();
        Search_Page search_page = new Search_Page(driver);
        Article_Page article_page = new Article_Page(driver);

        for (int i = 0; i < 4; i++) {
            driver.get("https://link.springer.com/search");
            search_page.searchArticle(Titles[i]);
            search_page.chooseArticle(1);
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            Assertions.assertEquals(Titles[i], article_page.getTitleName());
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            Assertions.assertEquals(Dois[i], article_page.getDOI());
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            Assertions.assertEquals(Dates[i], article_page.getPublishedDate());
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        }
    }
}

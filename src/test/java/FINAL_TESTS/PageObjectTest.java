package FINAL_TESTS;

import cz.cvut.fel.ts1.page_objects.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class PageObjectTest {

    static WebDriver driver;
    private String[] DOIs;

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\kristov\\Documents\\ts1-hw03-selenium\\src\\chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://link.springer.com/advanced-search");

        String allWords = "Page Object Model";
        String leastWords = "Selenium Testing";
        String startYear = "2021";
        String endYear = "2021";

        AdvancedSearch_Page advancedSearch_page = new AdvancedSearch_Page(driver);
        advancedSearch_page.fillForm(allWords, leastWords, startYear, endYear);
        advancedSearch_page.cookieBtn();
        advancedSearch_page.clickBtn();

        String contentType = "Article";
        String[] DOIs = new String[4];
        String[] Dates = new String[4];
        String[] Titles = new String[4];
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Search_Page search_page = new Search_Page(driver);
        Article_Page article_page = new Article_Page(driver);
        search_page.chooseContentTypeOfResults(contentType);

        String articles = driver.getCurrentUrl();

        for (int i = 1; i < 5; i++) {
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            search_page.chooseArticle(i);
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            DOIs[i-1] = article_page.getDOI();
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            Dates[i-1] = article_page.getPublishedDate();
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            Titles[i-1] = article_page.getTitleName();
            driver.navigate().to(articles);
        }
    }
}
